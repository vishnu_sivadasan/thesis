#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass report
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Introduction
\end_layout

\begin_layout Section
Background 
\end_layout

\begin_layout Subsection
Floods In India
\end_layout

\begin_layout Standard
Man made climatic changes due to deforestation, industrialization and pollution
 have triggered an array of disastrous events in nature.During the period
 from 2000 - 2015, we have seen an increasing number of natural disasters
 in Indian peninsula, especially water related ones.
 In the last ten yeas, a number of extreme rainfall events creating floods
 and mudslides have hit the country, causing thousands of casualties.
 The recent flood events in Jammu & Kashmir, Uttarakhand, Assam, Orissa
 etc are testaments to the fact that the climate is changing.
 These floods are grim reminders of the devastating effects of climate change.
 Countless lives are lost, everywhere, every year to these natural disasters.
 One of the main causes that makes these flood events so devastating is
 the lack of preparedness.
 Climatic models suggest that the India will be hit more and more by extreme
 flood events due to unprecedented rainfalls and if we are not prepared
 enough, loss of lives will even be higher.
 
\end_layout

\begin_layout Standard
This points out to the need of an efficient system for better flood management.
 Here arises the necessity for a flood prediction model, one that can take
 into account the rainfall and terrain data for the simulation, and predict
 the flood propagation so that necessary flood prevention and management
 steps can be decided and carried out.
 There are computational models that are available for this purpose, as
 discussed in section 
\begin_inset CommandInset ref
LatexCommand ref
reference "sub:Computational-models-available"

\end_inset

.
\end_layout

\begin_layout Subsection
Computational models available
\begin_inset CommandInset label
LatexCommand label
name "sub:Computational-models-available"

\end_inset


\end_layout

\begin_layout Standard
The equations that govern fluid flow for flood simulations currently do
 not have analytical solutions for most cases.
 Hence it solutions are found at discrete points of the flow domain and
 results are interpolated from these points.
 According to the nature of these interpolation points, the models for flood
 simulation are classified into two types.
\end_layout

\begin_layout Subsubsection
Eulerian Models
\end_layout

\begin_layout Standard
In Eulerian models, the interpolation points are stationary, and usually
 arranged in a gridded fashion for interpolation.
 In this method, the entire flow domain will be discretized into a grid/mesh,
 and solutions to the flow is calculated at every point in the grid.
 Generally, such methods area called meshed/gridded schemes (eg: FEM, FDM,
 FVM) 
\end_layout

\begin_layout Subsubsection
Lagrangian Models 
\end_layout

\begin_layout Standard
Lagrangian models use a different approach towards discretization of the
 flow domain.
 Here, the interpolation points move along with fluid, and are not stationary
 like the ones used in Eulerian schemes.
 The interpolation points are referred to as particles, and solution to
 the flow is calculated at the particles positions (eg: SPH).
 In this thesis, the Lagrangian method called SPH(Smoothed Particle Hydrodynamic
s) will be used for solving the flow problem.
\end_layout

\begin_layout Subsection
Related Works
\end_layout

\begin_layout Subsection
Why SPH
\end_layout

\begin_layout Section
Motivation
\end_layout

\begin_layout Section
Objectives
\end_layout

\begin_layout Section
Organization of thesis
\end_layout

\end_body
\end_document
